package com.blaze.demo.dao.jpa;

import com.blaze.demo.domain.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Repository can be used to delegate CRUD operations against the data source: http://goo.gl/P1J8QH
 */
public interface CustomerRepository extends PagingAndSortingRepository<Customer, Long> {
    Customer findCustomerByCity(String city);
    Page findAll(Pageable pageable);
}
