import { Injectable } from '@angular/core';

@Injectable()
export class VariableService {
  constructor(){ }  
  DOMAIN = 'http://35.166.180.250:8080/example/v1/';
  ALLCUSTOMERSDATA = this.DOMAIN + 'customers?page=0&size=100';
  GETCUSTOMERBYID = this.DOMAIN + 'customers/';
  ADDNEWCUSTOMER = this.DOMAIN + 'customers';
  DELETECUSTOMERBYID = this.DOMAIN + 'customers/';
  UPDATECUSTOMERBYID = this.DOMAIN + 'customers/';
}
