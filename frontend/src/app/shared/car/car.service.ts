import { Injectable } from '@angular/core';
import { VariableService } from '../variables/variable.service';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {Http,RequestOptions,Headers } from '@angular/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class CarService {

  constructor(private http: HttpClient,private UrlVariable:VariableService) {}

  /* get all customer data, used for populating the datatable */  
  getData(){
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('authentication', `hello`);

    const options = new RequestOptions({headers: headers});
    return this.http.get<any>(this.UrlVariable.ALLCUSTOMERSDATA)
    .toPromise()
    .then(res => res.content);
  } 
}


