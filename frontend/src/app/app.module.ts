import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { CarService } from './shared/car/car.service';
import { HttpClientModule, HttpRequest , HttpHeaders } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { CarListComponent } from './car-list/car-list.component';
import { TableModule, Table } from 'primeng/table';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import { NewCustomerComponent } from './new-customer/new-customer.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule , MatProgressSpinnerModule } from '@angular/material';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { BackdropComponent } from './backdrop/backdrop.component';
import { MatDialogModule } from '@angular/material/dialog';
import { DiaglogDataComponent } from './diaglog-data/diaglog-data.component';
import { CustService } from './shared/car/cust.service';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { SnackbarComponent } from './snackbar/snackbar.component';
import { VariableService } from './shared/variables/variable.service';

const routes: Routes = [
  { path : '', component: BackdropComponent },
  { path :'list', component: CarListComponent},
  { path : 'newCustomer', component: NewCustomerComponent },
  { path : 'editCustomer/:id', component: NewCustomerComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    CarListComponent,
    NewCustomerComponent,
    BackdropComponent,
    DiaglogDataComponent,
    SnackbarComponent
  ],
  entryComponents: [DiaglogDataComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    HttpClientModule,
    MatTableModule,
    MatPaginatorModule,
    MatCardModule,
    MatButtonModule,
    RouterModule.forRoot(routes),
    MatFormFieldModule,
    MatInputModule,
    MatProgressSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFontAwesomeModule,
    MatDialogModule,
    MatToolbarModule,
    MatSnackBarModule
  ],
  providers: [
    CarService,
    CustService,
    VariableService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

