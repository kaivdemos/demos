import { Component, OnInit,Inject } from '@angular/core';
import { FormGroup,FormControl } from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
import { CarService } from '../shared/car/car.service';
import { CustService } from '../shared/car/cust.service';
import { HttpClient } from '@angular/common/http';
import { SnackbarComponent } from '../snackbar/snackbar.component';

export interface Customer {
  id: string;
  name: String;
  city: String;
  rating: number;
  description:String;
}

class CustClass {
  constructor(public id:string = '',
              public name:string = '',
              public city:string = '',
              public rating:string = '',
              public description:string = '') {
  }
}

@Component({
  selector: 'app-new-customer',
  templateUrl: './new-customer.component.html',
  styleUrls: ['./new-customer.component.css']
})
export class NewCustomerComponent implements OnInit {
  customerData:Customer[];
  customer=[];
  param:String;
  urlString:String;
  urlParam=[];
  title:String;

  constructor(private router:Router,private activatedRoute:ActivatedRoute,private custService:CustService,private http:HttpClient) {
    this.activatedRoute.params.subscribe( params => params)
    this.urlString=this.router.url;

   }
  
  model: CustClass = new CustClass();
 
  postForm=function(){
    this.custService.createCustomer(this.model).then(customerData => {
     this.router.navigate(['list']);
    });   
  }

  submitForm = function(){
    this.postForm();
  }

  goBack = function(){
    this.router.navigate(['list']);
  }

  setFormData(custId){
   this.custService.getCustData(custId).then(custForID =>{
        this.model=custForID;
      }); 
  }

  ngOnInit() {
    this.urlParam=this.urlString.split('/');
    if (this.urlParam[1]==='editCustomer') {
      this.title="Update Customer Data"
      this.setFormData(this.urlParam[2]);
    }else{
      this.title="Create New Customer"
    }
  }
}
