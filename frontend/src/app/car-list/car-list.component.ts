import { Component, OnInit , ViewChild,Inject } from '@angular/core';
import {CarService} from '../shared/car/car.service';
import { HttpClient } from '@angular/common/http';
import {Http,RequestOptions,Headers } from '@angular/http';
import { MatPaginator, MatTableModule, MatTableDataSource } from '@angular/material';
import { MatDialog,MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router,ActivatedRoute } from '@angular/router';
import { DiaglogDataComponent } from '../diaglog-data/diaglog-data.component';

export interface Customer {
  id: string;
  name: String;
  city: String;
  rating: number;
  description:String;
}

@Component({
  selector: 'app-car-list',
  templateUrl: './car-list.component.html',
  styleUrls: ['./car-list.component.css']
})

export class CarListComponent implements OnInit {
  customerData:Customer[];
  customer=[];
  custForID:Customer[];
  cols=[];
  displayedColumns=[];
  dataSource:MatTableDataSource<Customer>;
  resourcesLoaded=false;
  urlString:String;
  urlParam=[];


  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private http:Http,private carService: CarService,private router:Router,public dialog: MatDialog) {
    this.urlString=this.router.url;
  }

    /* Service call to populate the datatable */
    getCustomerData(){    
      this.carService.getData().then(customerData => {
        this.customer=customerData;
        this.setData();
      });    
    }   
    setData(){
      this.displayedColumns = ['edit', 'name', 'city','rating'];
      this.dataSource = new MatTableDataSource(this.customer) ; // assign data to the datasource, data to be generated in the table  
      this.dataSource.paginator=this.paginator;
    }


    /* navigation on click of create new customer */
    createCustomer = function(){
      this.router.navigate(['newCustomer']);
    }  

    /* Service call to get particular customer data */
    edit = function(param){
      this.sendData(param);
    }    
    sendData = function(custId){ 
      this.router.navigate(['editCustomer/'+custId]);
      
    }
   

    /* launch dialog box to delete customer and call service for the same */ 
    deleteCustomer = function(custId){
      const dialogRef=this.dialog.open(DiaglogDataComponent, {
        width: '500px',
        data: {
          customerId: custId
        }
      });
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
      });       
    }

    ngOnInit() {
      this.getCustomerData(); 
    } //end of ngOnInit
} //end of CarListComponent





