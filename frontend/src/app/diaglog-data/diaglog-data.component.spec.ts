import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiaglogDataComponent } from './diaglog-data.component';

describe('DiaglogDataComponent', () => {
  let component: DiaglogDataComponent;
  let fixture: ComponentFixture<DiaglogDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiaglogDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiaglogDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
